package parser

import (
	"gitlab.com/gascoigne/boolexpr/expr"
	"gitlab.com/gascoigne/boolexpr/grammar"
	"gitlab.com/gascoigne/pogo"
)

type exprBuilder struct{}

func (*exprBuilder) VisitExpr(delegate grammar.BoolexprVisitor, items []pogo.Parsed) expr.Expr {
	return grammar.AcceptDisjunction(delegate, items[0])
}

func (*exprBuilder) VisitDisjunction(delegate grammar.BoolexprVisitor, items []pogo.Parsed) expr.Expr {
	cjs := grammar.AllConjunctions(delegate, items)
	xs := make([]expr.Expr, len(cjs))
	copy(xs, cjs)

	// Flatten nested disjunctions
	newXs := make([]expr.Expr, 0)
	for _, x := range xs {
		if x2, ok := x.(expr.Disjunction); ok {
			newXs = append(newXs, x2...)
		} else {
			newXs = append(newXs, x)
		}
	}
	xs = newXs

	if len(xs) == 1 {
		return xs[0]
	}

	return expr.Disjunction(xs)
}

func (*exprBuilder) VisitConjunction(delegate grammar.BoolexprVisitor, items []pogo.Parsed) expr.Expr {
	ngs := grammar.AllNegations(delegate, items)
	xs := make([]expr.Expr, len(ngs))
	copy(xs, ngs)

	// Flatten nested conjunctions
	newXs := make([]expr.Expr, 0)
	for _, x := range xs {
		if x2, ok := x.(expr.Conjunction); ok {
			newXs = append(newXs, x2...)
		} else {
			newXs = append(newXs, x)
		}
	}
	xs = newXs

	if len(xs) == 1 {
		return xs[0]
	}

	return expr.Conjunction(xs)
}

func (*exprBuilder) VisitNegation(delegate grammar.BoolexprVisitor, items []pogo.Parsed) expr.Expr {
	t := grammar.AcceptTerm(delegate, items[1])
	if items[0] == pogo.NilParsed {
		return t
	}

	// Simplify double negations
	if x, ok := t.(expr.Negation); ok {
		return x.X
	}

	return expr.Negation{
		X: t,
	}
}

func (*exprBuilder) VisitTerm(delegate grammar.BoolexprVisitor, items []pogo.Parsed) expr.Expr {
	if p, ok := grammar.IsAtom(items[0]); ok {
		return grammar.AcceptAtom(delegate, p)
	}

	return grammar.AcceptDisjunction(delegate, items[1])
}

func (*exprBuilder) VisitAtom(delegate grammar.BoolexprVisitor, items []pogo.Parsed) expr.Atom {
	return expr.Atom(items[0].(pogo.Item).Value)
}
