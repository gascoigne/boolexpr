package parser

import (
	"bytes"
	"fmt"
	"io"

	"gitlab.com/gascoigne/boolexpr/expr"
	"gitlab.com/gascoigne/boolexpr/grammar"
	"gitlab.com/gascoigne/pogo"
)

type ParserImpl struct {
	pogo.BaseParseState
	lexer pogo.LexerIface
}

func newParser(r io.Reader) *ParserImpl {
	return &ParserImpl{
		lexer: grammar.NewBufferedLexer(r),
	}
}

func (p *ParserImpl) HandleError(tok pogo.Item, err error) {
	fmt.Printf("error reported at %v: %v\n", tok, err)
}

func (p *ParserImpl) Clone() pogo.ParseState {
	return &ParserImpl{
		BaseParseState: p.BaseParseState.CloneBase(),
		lexer:          p.lexer,
	}
}

func (p *ParserImpl) Lexer() pogo.LexerIface {
	return p.lexer
}

func Parse(input string) (expr.Expr, error) {
	parser := newParser(bytes.NewBufferString(input))

	tree, state := pogo.Do(pogo.Root(&grammar.Expr), parser)
	for _, err := range state.Errors() {
		parser.HandleError(err.Token, err.Error)
	}

	ast := grammar.AcceptExpr(new(exprBuilder), tree)
	// fmt.Printf("parse tree %v\n", tree)
	// spew.Dump(ast)
	return ast, nil
}
