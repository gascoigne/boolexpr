package parser_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gascoigne/boolexpr/expr"
	"gitlab.com/gascoigne/boolexpr/parser"
)

type TestCase struct {
	Input  string
	Output expr.Expr
}

var testCases = []TestCase{
	{
		Input:  `bar`,
		Output: expr.Atom("bar"),
	},
	{
		Input: `NOT bar`,
		Output: expr.Negation{
			X: expr.Atom("bar"),
		},
	},
	{
		Input: `foo AND bar`,
		Output: expr.Conjunction{
			expr.Atom("foo"),
			expr.Atom("bar"),
		},
	},
	{
		Input: `123 AND 456`,
		Output: expr.Conjunction{
			expr.Atom("123"),
			expr.Atom("456"),
		},
	},
	{
		Input: `some_special AND characters-0`,
		Output: expr.Conjunction{
			expr.Atom("some_special"),
			expr.Atom("characters-0"),
		},
	},
	{
		Input: `foo OR bar`,
		Output: expr.Disjunction{
			expr.Atom("foo"),
			expr.Atom("bar"),
		},
	},
	{
		Input: `foo AND NOT bar`,
		Output: expr.Conjunction{
			expr.Atom("foo"),
			expr.Negation{
				X: expr.Atom("bar"),
			},
		},
	},
	{
		Input: `foo AND NOT (bar OR baz)`,
		Output: expr.Conjunction{
			expr.Atom("foo"),
			expr.Negation{
				X: expr.Disjunction{
					expr.Atom("bar"),
					expr.Atom("baz"),
				},
			},
		},
	},
	{
		Input: `foo OR NOT (bar AND baz)`,
		Output: expr.Disjunction{
			expr.Atom("foo"),
			expr.Negation{
				X: expr.Conjunction{
					expr.Atom("bar"),
					expr.Atom("baz"),
				},
			},
		},
	},
	{
		Input: `foo AND bar AND baz`,
		Output: expr.Conjunction{
			expr.Atom("foo"),
			expr.Atom("bar"),
			expr.Atom("baz"),
		},
	},
	{
		Input: `(foo AND bar) AND baz`,
		Output: expr.Conjunction{
			expr.Atom("foo"),
			expr.Atom("bar"),
			expr.Atom("baz"),
		},
	},
	{
		Input: `(foo OR bar) OR baz`,
		Output: expr.Disjunction{
			expr.Atom("foo"),
			expr.Atom("bar"),
			expr.Atom("baz"),
		},
	},
	{
		Input:  `NOT (NOT foo)`,
		Output: expr.Atom("foo"),
	},
	{
		Input: `(foo OR bar) AND (baz OR quux)`,
		Output: expr.Conjunction{
			expr.Disjunction{
				expr.Atom("foo"),
				expr.Atom("bar"),
			},
			expr.Disjunction{
				expr.Atom("baz"),
				expr.Atom("quux"),
			},
		},
	},
}

func TestParser(t *testing.T) {
	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			actual, err := parser.Parse(tc.Input)
			assert.NoError(t, err, "error while parsing: %v", tc.Input)
			assert.EqualValues(t, actual, tc.Output, "error while parsing: %v", tc.Input)
		})
	}
}
