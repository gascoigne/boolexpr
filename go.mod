module gitlab.com/gascoigne/boolexpr

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/gascoigne/pogo v0.1.3
	go.mongodb.org/mongo-driver v1.5.4 // indirect
)
