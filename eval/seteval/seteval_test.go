package seteval_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gascoigne/boolexpr/eval/seteval"
	"gitlab.com/gascoigne/boolexpr/expr"
	"gitlab.com/gascoigne/boolexpr/parser"
)

type NamedSet struct {
	Name string
	seteval.MapSet
}

func NewNamedSet(name string, elems ...expr.Atom) *NamedSet {
	s := &NamedSet{
		Name:   name,
		MapSet: make(seteval.MapSet),
	}
	for _, e := range elems {
		s.Add(e)
	}
	return s
}

type TestCase struct {
	Expr       string
	InputSets  []seteval.Set
	OutputSets []string
}

var sets = []seteval.Set{
	NewNamedSet("a", expr.Atom("foo")),
	NewNamedSet("b", expr.Atom("foo"), expr.Atom("bar")),
	NewNamedSet("c", expr.Atom("bar"), expr.Atom("baz")),
	NewNamedSet("d", expr.Atom("foo"), expr.Atom("baz")),
	NewNamedSet("e", expr.Atom("foo"), expr.Atom("quux")),
	NewNamedSet("f", expr.Atom("foo"), expr.Atom("bar"), expr.Atom("baz"), expr.Atom("quux")),
	NewNamedSet("g", expr.Atom("quux")),
}

var testCases = []TestCase{
	{
		Expr:       `bar`,
		InputSets:  sets,
		OutputSets: []string{"b", "c", "f"},
	},
	{
		Expr:       `NOT bar`,
		InputSets:  sets,
		OutputSets: []string{"a", "d", "e", "g"},
	},
	{
		Expr:       `foo AND bar`,
		InputSets:  sets,
		OutputSets: []string{"b", "f"},
	},
	{
		Expr:       `foo OR bar`,
		InputSets:  sets,
		OutputSets: []string{"a", "b", "c", "d", "e", "f"},
	},
	{
		Expr:       `foo AND NOT bar`,
		InputSets:  sets,
		OutputSets: []string{"a", "d", "e"},
	},
	{
		Expr:       `foo AND NOT (bar OR baz)`,
		InputSets:  sets,
		OutputSets: []string{"a", "e"},
	},
	{
		Expr:       `foo OR NOT (bar AND baz)`,
		InputSets:  sets,
		OutputSets: []string{"a", "b", "d", "e", "f", "g"},
	},
	{
		Expr:       `foo AND bar AND baz`,
		InputSets:  sets,
		OutputSets: []string{"f"},
	},
	{
		Expr:       `(foo AND bar) AND baz`,
		InputSets:  sets,
		OutputSets: []string{"f"},
	},
	{
		Expr:       `(foo OR bar) OR baz`,
		InputSets:  sets,
		OutputSets: []string{"a", "b", "c", "d", "e", "f"},
	},
	{
		Expr:       `NOT (NOT foo)`,
		InputSets:  sets,
		OutputSets: []string{"a", "b", "d", "e", "f"},
	},
	{
		Expr:       `(foo OR bar) AND (baz OR quux)`,
		InputSets:  sets,
		OutputSets: []string{"c", "d", "e", "f"},
	},
}

func TestSetEval(t *testing.T) {
	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			expr, err := parser.Parse(tc.Expr)
			assert.NoError(t, err, "error while parsing: %v", tc.Expr)

			actualSets := seteval.Search(expr, tc.InputSets)
			actualSetNames := make([]string, len(actualSets))
			for i, s := range actualSets {
				actualSetNames[i] = s.(*NamedSet).Name
			}
			assert.ElementsMatch(t, tc.OutputSets, actualSetNames, "search \"%v\" returned incorrect results", tc.Expr)
		})
	}
}
