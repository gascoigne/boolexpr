package seteval

import "gitlab.com/gascoigne/boolexpr/expr"

type Set interface {
	Has(expr.Atom) bool
}

func Search(e expr.Expr, s []Set) []Set {
	results := make([]Set, 0)
	for _, s := range s {
		if Eval(e, s) {
			results = append(results, s)
		}
	}
	return results
}

func Eval(e expr.Expr, s Set) bool {
	switch e := e.(type) {
	case expr.Disjunction:
		return evalDisjunction(e, s)
	case expr.Conjunction:
		return evalConjunction(e, s)
	case expr.Negation:
		return evalNegation(e, s)
	case expr.Atom:
		return s.Has(e)
	default:
		panic("unreachable")
	}
}

func evalDisjunction(e expr.Disjunction, s Set) bool {
	for _, e := range e {
		if Eval(e, s) {
			return true
		}
	}
	return false
}

func evalConjunction(e expr.Conjunction, s Set) bool {
	for _, e := range e {
		if !Eval(e, s) {
			return false
		}
	}
	return true
}

func evalNegation(e expr.Negation, s Set) bool {
	return !Eval(e.X, s)
}

type MapSet map[expr.Atom]*struct{}

func (s MapSet) Has(a expr.Atom) bool {
	_, ok := s[a]
	return ok
}

func (s MapSet) Add(a expr.Atom) {
	s[a] = nil
}

func (s MapSet) Del(a expr.Atom) {
	delete(s, a)
}
