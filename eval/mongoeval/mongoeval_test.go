package mongoeval_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/gascoigne/boolexpr/eval/mongoeval"
	"gitlab.com/gascoigne/boolexpr/parser"
)

type TestCase struct {
	Expr  string
	Query bson.M
}

var testCases = []TestCase{
	{
		Expr:  `bar`,
		Query: bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
	},
	{
		Expr: `NOT bar`,
		Query: bson.M{
			"$nor": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
			},
		},
	},
	{
		Expr: `foo AND bar`,
		Query: bson.M{
			"$and": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
				bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
			},
		},
	},
	{
		Expr: `foo OR bar`,
		Query: bson.M{
			"$or": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
				bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
			},
		},
	},
	{
		Expr: `foo AND NOT bar`,
		Query: bson.M{
			"$and": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
				bson.M{"$nor": bson.A{
					bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
				}},
			},
		},
	},
	{
		Expr: `foo AND NOT (bar OR baz)`,
		Query: bson.M{
			"$and": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
				bson.M{"$nor": bson.A{
					bson.M{"$or": bson.A{
						bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
						bson.M{"tags": bson.M{"$all": bson.A{"baz"}}},
					}},
				}},
			},
		},
	},
	{
		Expr: `foo OR NOT (bar AND baz)`,
		Query: bson.M{
			"$or": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
				bson.M{"$nor": bson.A{
					bson.M{
						"$and": bson.A{
							bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
							bson.M{"tags": bson.M{"$all": bson.A{"baz"}}},
						},
					},
				}},
			},
		},
	},
	{
		Expr: `foo AND bar AND baz`,
		Query: bson.M{
			"$and": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
				bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
				bson.M{"tags": bson.M{"$all": bson.A{"baz"}}},
			},
		},
	},
	{
		Expr: `(foo AND bar) AND baz`,
		Query: bson.M{
			"$and": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
				bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
				bson.M{"tags": bson.M{"$all": bson.A{"baz"}}},
			},
		},
	},
	{
		Expr: `(foo OR bar) OR baz`,
		Query: bson.M{
			"$or": bson.A{
				bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
				bson.M{"tags": bson.M{"$all": bson.A{"bar"}}},
				bson.M{"tags": bson.M{"$all": bson.A{"baz"}}},
			},
		},
	},
	{
		Expr:  `NOT (NOT foo)`,
		Query: bson.M{"tags": bson.M{"$all": bson.A{"foo"}}},
	},
	{
		Expr: `(foo OR bar) AND (baz OR quux)`,
		Query: bson.M{
			"$and": bson.A{
				bson.M{
					"$or": bson.A{
						bson.M{
							"tags": bson.M{"$all": bson.A{"foo"}},
						}, bson.M{
							"tags": bson.M{"$all": bson.A{"bar"}},
						},
					},
				}, bson.M{
					"$or": bson.A{
						bson.M{"tags": bson.M{"$all": bson.A{"baz"}}},
						bson.M{"tags": bson.M{"$all": bson.A{"quux"}}},
					},
				},
			},
		},
	},
}

func TestSetEval(t *testing.T) {
	for _, tc := range testCases {
		t.Run("", func(t *testing.T) {
			expr, err := parser.Parse(tc.Expr)
			assert.NoError(t, err, "error while parsing: %v", tc.Expr)

			actualQuery := mongoeval.BuildQuery(expr, "tags")
			buf, err := bson.MarshalExtJSON(actualQuery, true, false)
			assert.NoError(t, err)

			assert.EqualValues(t, tc.Query, actualQuery, "search \"%v\" returned incorrect query: %v", tc.Expr, string(buf))
		})
	}
}
