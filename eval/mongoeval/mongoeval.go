package mongoeval

import (
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/gascoigne/boolexpr/expr"
)

func BuildQuery(e expr.Expr, field string) bson.M {
	switch e := e.(type) {
	case expr.Disjunction:
		exprs := make(bson.A, len(e))
		for i, e := range e {
			exprs[i] = BuildQuery(e, field)
		}
		return bson.M{
			"$or": exprs,
		}
	case expr.Conjunction:
		exprs := make(bson.A, len(e))
		for i, e := range e {
			exprs[i] = BuildQuery(e, field)
		}
		return bson.M{
			"$and": exprs,
		}
	case expr.Negation:
		return bson.M{
			"$nor": bson.A{BuildQuery(e.X, field)},
		}
	case expr.Atom:
		return bson.M{
			field: bson.M{
				"$all": bson.A{string(e)},
			},
		}
	default:
		panic("unreachable")
	}
}
