package grammar

import (
	"gitlab.com/gascoigne/boolexpr/expr"
	. "gitlab.com/gascoigne/pogo"
)

//go:generate nex -e boolexpr.nex
//go:generate go run ./cmd/visitor-gen/main.go -generate-visitor -path . -public -pkg grammar

var Expr,
	disjunction,
	conjunction,
	negation,
	term,
	atom Parser

func init() {
	Expr = TypedProdIface("Expr", (*expr.Expr)(nil), Seq(&disjunction, EOF))

	disjunction = TypedProdIface("Disjunction", (*expr.Expr)(nil), Seq(&conjunction, Many(Seq(Tok(KW_OR), &conjunction))))

	conjunction = TypedProdIface("Conjunction", (*expr.Expr)(nil), Seq(&negation, Many(Seq(Tok(KW_AND), &negation))))

	negation = TypedProdIface("Negation", (*expr.Expr)(nil), Seq(Maybe(Tok(KW_NOT)), &term))

	term = TypedProdIface("Term", (*expr.Expr)(nil), Or(&atom, Seq(Char('('), &disjunction, Char(')'))))

	atom = TypedProd("Atom", expr.Atom(""), Tok(ATOM))
}
