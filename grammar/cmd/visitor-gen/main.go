package main

import (
	"flag"

	_ "gitlab.com/gascoigne/boolexpr/grammar"
	"gitlab.com/gascoigne/pogo"
)

var (
	path = flag.String("path", "", "")
	pkg  = flag.String("pkg", "", "")
)

func main() {
	flag.Parse()
	pogo.VisitorTemplate.Generate("boolexpr", *pkg, *path)
}
