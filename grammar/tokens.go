package grammar

import "gitlab.com/gascoigne/pogo"

const (
	ATOM   = pogo.TokenType("ATOM")
	KW_AND = pogo.TokenType("KW_AND")
	KW_OR  = pogo.TokenType("KW_OR")
	KW_NOT = pogo.TokenType("KW_NOT")
)
