// Package expr contains the internal representation of an expression
package expr

// Expr is the root node of an expression tree.
type Expr interface{ isExpr() }

// A Disjunction is a logical or of one or more subexpressions.
type Disjunction []Expr

func (Disjunction) isExpr() {}

// A Conjunction is a logical and of one or more subexpressions.
type Conjunction []Expr

func (Conjunction) isExpr() {}

// A Negation is an logical not of a subexpression.
type Negation struct {
	X Expr
}

func (Negation) isExpr() {}

// A Atom is an atomic expression, matching a named boolean variable.
type Atom string

func (Atom) isExpr() {}
